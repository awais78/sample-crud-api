# README #

This is a sample CRUD REST API.

### How do I get set up? ###

* you must have serverless installed on your system
* clone respo on your system
* import database from db/app_booking.sql file in your system and add credentails in utils/coniguration.js
* run command `npm install` on cmd or bash
* update event.json file to input
* to run a function use this commant `serverless invoke local -f users -p event.json`