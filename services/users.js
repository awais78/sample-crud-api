'use strict'
const mysql = require('../utils/configuration');
const utils = require('../utils/utils');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const pool = mysql.pool;

module.exports.handler = (event, context, callback) => {
	if(event.httpMethod == "GET") {
		 get_user(event, context, callback);
	} else if(event.httpMethod == "POST") {
         save_user(event, context, callback);
    } else if(event.httpMethod == "PUT") {
         update_user(event, context, callback);
    } else if(event.httpMethod == "DELETE") {
         delete_user(event, context, callback);
    } else {
        utils.respond(context, 400, {message: "Method does not exist."});
    }
}

const get_user = (event, context, callback) => {
    let query = null;
    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        query = 'select * from users';
    } else {
        query = 'select * from users where id = "'+event.pathParameters.id+'"';
    }

    pool.getConnection(function(err, conn) {
        if(err) {
            utils.respond(context, 500, {message: err.message});
        } else {
            conn.query(query, function(err, u) {
                conn.destroy();
                if(err) {
                    utils.respond(context, 500, {message: err.message});
                } else {
                    utils.respond(context, 200, u);
                }
            });
        }
    });
}

const save_user = (event, context, callback) => {
    //let body = JSON.parse(event.body);
    let body = event.body;

    pool.getConnection(function(err, conn) {
        if(err) {
            utils.respond(context, 500, {message: err.message});
        } else {
            let user = {
                id: uuidv1(),
                name: body.name,
                email: body.email,
                city: body.city,
                country: body.country,
                created_at: moment().format("YYYY-MM-DD HH:mm:ss")
            };

            conn.query('insert into users set ?', [user], function(err, res) {
                conn.destroy();
                if(err) {
                    utils.respond(context, 500, {message: err.message});
                } else {
                    utils.respond(context, 200, {message: "User has been created successfully.", userId: user.id});
                }
            });
        }
    });
}

const update_user = (event, context, callback) => {
    //let body = JSON.parse(event.body);
    let body = event.body;

    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        utils.respond(context, 400, {message: "Mandatory parameters are missing."});
    } else {
        pool.getConnection(function(err, conn) {
            if(err) {
                utils.respond(context, 500, {message: err.message});
            } else {
                conn.query('update users set ? where id = ?', [body, event.pathParameters.id], function(err, res) {
                    conn.destroy();
                    if(err) {
                        utils.respond(context, 500, {message: err.message});
                    } else {
                        utils.respond(context, 200, {message: "User has been updated successfully."});
                    }
                });
            }
        });
    }
}

const delete_user = (event, context, callback) => {
    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        utils.respond(context, 400, {message: "Mandatory parameters are missing."});
    } else {
        pool.getConnection(function(err, conn) {
            if(err) {
                utils.respond(context, 500, {message: err.message});
            } else {
                conn.query('delete from users where id = ?', [event.pathParameters.id], function(err, res) {
                    conn.destroy();
                    if(err) {
                        utils.respond(context, 500, {message: err.message});
                    } else {
                        utils.respond(context, 200, {message: "User has been deleted successfully."});
                    }
                });
            }
        });
    }
}