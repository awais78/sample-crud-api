'use strict'
var mysql= require('mysql');

module.exports.pool = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : '',
  connectionLimit : 300,
  database: 'app_booking'
});