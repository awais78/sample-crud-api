'use strict'

module.exports.respond = (context, statusCode, content) => {
	const response = {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(content)
    };
    context.succeed(response);
}